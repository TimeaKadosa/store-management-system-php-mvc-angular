<?php

namespace CmsMvc\Models;

use CmsMvc\Models\BaseModel;
use CmsMvc\Services\Database;

	class UserTypesModel extends BaseModel {

		# 1 set the table name
		protected $table = "user_types";

		# 2 set the validation rules
		protected $validationRules = [
			'id'		=> 'numeric',
			'name'		=> 'required',
		];


	}