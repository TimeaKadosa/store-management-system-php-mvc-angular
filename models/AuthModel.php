<?php

namespace CmsMvc\Models;

use CmsMvc\Services\Database;
use CmsMvc\Services\Password;

	class AuthModel {

		# 1 set the table name
		protected $table = "users";

		public function authUser($username, $password) {

			$sql = "SELECT * FROM " . $this->table . " WHERE username = :username AND password = :password";
			$stmt = Database::conn()->prepare($sql);
			
			$bind = ['username' => $username, 'password' => $password];
			# Execute query
			$stmt->execute($bind);
			
			# Fetch a row
			$row = $stmt->fetch();

			# Return the row
			return $row;

		}

	}