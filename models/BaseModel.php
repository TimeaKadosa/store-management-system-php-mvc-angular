<?php

namespace CmsMvc\Models;

use CmsMvc\Services\Database;
use CmsMvc\Services\Password;
use \Exception;

	class BaseModel {

		private $Database;

		protected $table;

		public $error;

		public function __construct() {


		}

		public function fetchAll () {

			# Prepare and execute query
			$sql = "SELECT * FROM " . $this->table;
			$stmt = Database::conn()->prepare($sql);
			$stmt->execute();
			
			# Fetch all records into an array
			$collection = $stmt->fetchAll();

			# Return the collection
			return $collection;
		}

		public function fetch($id) {

			$sql = "SELECT * FROM " . $this->table . " WHERE id = :id";
			$stmt = Database::conn()->prepare($sql);
			
			# Execute query
			$stmt->execute(array('id' => $id));
			
			# Fetch a row
			$row = $stmt->fetch();

			# Return the row
			return $row;


		}

		public function insert($input) {

			try{

				if (isset($input['password'])) {

					$input['password'] = Password::password_encrypt($input['password']);
				}
				

				# Build insert columns and prepared values
				$columns 	= implode(', ', array_keys($input));
				
				$prepared 	= ':' . implode(', :', array_keys($input));

				# Prepare query
				$sql = "INSERT INTO " . $this->table . " (" . $columns . ") VALUES (" . $prepared . ")";
				$db = Database::conn()->prepare($sql);

				# Execute query
				if($db->execute($input)) {

					return true;

				}
			}
			catch(Exception $e){

				 if ($e->getCode() == 23000) {
				 	
				 		$this->error = "The email is already exists";
				 }
			}
		}



		public function update ($id, $input) {

			if (isset($input['password'])) {

				unset($input['password_c']);
				
				$input['password'] = Password::password_encrypt($input['password']);
			}
			

			$update = implode(",", array_map(function($a, $b) { return sprintf(" $b = '$a' ");}, $input , array_keys($input)));
			# Prepare query

			$sql = "UPDATE " . $this->table . " SET " . $update . " WHERE id = " . $id;

			$stmt = Database::conn()->prepare($sql);

			# Execute query
			if($stmt->execute($input)) {
				return true;
			} else {
				return false;
			}
		}

		public function destroy($id){

			
			$sql = "DELETE FROM " . $this->table . " WHERE id = ". 1;
			$stmt = Database::conn()->exec($sql);

			if($stmt === true)
		    {

		    	return true;
		    }	

		    return false;
					
		}


		public function getLastInsertId() {

			return $id = Database::conn()->lastInsertId();
		}

		public function execute($sql, $binding) {

			$db = Database::conn()->prepare($sql);
			$db->execute($binding);

			return true;
		}





	}