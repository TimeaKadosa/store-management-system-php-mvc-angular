<?php

namespace CmsMvc\Models;

use CmsMvc\Models\BaseModel;
use CmsMvc\Services\Database;
use CmsMvc\Services\Password;

	class UserModel extends BaseModel {

		# 1 set the table name
		protected $table = "users";

		# 2 set the validation rules
		protected $validationRules = [
			'id'		=> 'numeric',
			'name'		=> 'required',
			'email'		=> 'required|valid_email',
			'username'	=> 'required|min_len,4|max_len,16',
			'password'	=> 'min_len,4|max_len,16'
		];


	}