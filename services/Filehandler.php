<?php

namespace CmsMvc\Services;

	class Filehandler {

		public $filename;

		private $temp_path;

		public $img_upload_dir = "/profileImages";

		public $errors = array();

		protected $upload_errors = array(
			// http://www.php.net/manual/en/features.file-upload.errors.php
			UPLOAD_ERR_OK			=> "No errors.",
			UPLOAD_ERR_INI_SIZE		=> "Larger than upload_max_filesize.",
			UPLOAD_ERR_FORM_SIZE 	=> "Larger than form MAX_FILE_SIZE.",
			UPLOAD_ERR_PARTIAL 		=> "Partial upload.",
			UPLOAD_ERR_NO_FILE 		=> "No file.",
			UPLOAD_ERR_NO_TMP_DIR	=> "No temporary directory.",
			UPLOAD_ERR_CANT_WRITE	=> "Can't write to disk.",
			UPLOAD_ERR_EXTENSION 	=> "File upload stopped by extension."
		);


		public function __construct() {


		}


		public function attach_file($file) {  // $file = actual uploaded file

			// Perform error checking on the form parameters
			if(!$file || empty($file) || !is_array($file)) {
			// error: nothing uploaded or wrong argument usage
			$this->errors[] = "No file was uploaded.";
			return false;
			}
			elseif($file['error'] != 0) {
			// error: report what PHP says went wrong
			$this->errors[] = $this->upload_errors[$file['error']];
			return false;
			}

			else {
			// Set object attributes to the form parameters.
			$this->temp_path  = $file['tmp_name'];
			$this->filename   = basename($file['name']);
			//$this->$type       = $file['type'];
			//$this->$size       = $file['size'];

			return true;

			}

    }

		public function imageUpload($file){

			$img_upload_dir = PUBLIC_FOLDER.  "/profileImages";

			self::attach_file($file);

			// Can't save without filename and temp location
			if(empty($this->filename) || empty($this->temp_path)) {
				$this->errors[] = "The file location was not available.";
				return false;
			}

			# Determine the target_path
			$target_path = $img_upload_dir . "/" . $this->filename ;
			
			# Make sure a file doesn't already exist in the target location
			if(file_exists($target_path)) {

				$_SESSION['message']['error'] = "The file ". $this->filename ." already exists.";
				return false;
			}

			# Attempt to move the file 
	        if(move_uploaded_file($this->temp_path, $target_path)) {
	            // Success
	            // we need to get rid of these data,and then create()	
	            unset($this->errors);
	            unset($this->upload_errors);
	            unset($this->temp_path);
	            unset($this->img_upload_dir);
	        
	        }

	        return true;
        }

	}

