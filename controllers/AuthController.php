<?php

namespace CmsMvc\Controllers;

use CmsMvc\Models\AuthModel;

use Firebase\JWT\JWT;

use \DomainException;
use \InvalidArgumentException;
use \UnexpectedValueException;
use \DateTime;
use \Exception;

use CmsMvc\Services\Password;


class AuthController extends BaseController
{
	
		public function __construct() {

			$this->Model = new AuthModel();
			parent::__construct();
		}


	public function authenticate() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		# set the username and password (no validateing yet)
		$username = $request->username;
		$password = Password::password_encrypt($request->password);

		# Get the user by it's username and password
		$user = $this->Model->authUser($username, $password );

		if($user){

			$tokenId    = base64_encode(mcrypt_create_iv(32));
		    $issuedAt   = time();
		    $notBefore  = $issuedAt;             //Adding 10 seconds
		    //$expire     = $notBefore + 120;            // Adding 60 seconds
		    $serverName = $_SERVER['SERVER_NAME']; // Retrieve the server name from config file

		    /*
		     * Create the token as an array
		     */
		    $data = [
		        'iat'  => $issuedAt,         // Issued at: time when the token was generated
		        'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
		        'iss'  => $_SERVER['SERVER_NAME'],       // Issuer
		        'nbf'  => $notBefore,        // Not before
		       //'exp'  => $expire,           // Expire
		        'data' => [                  // Data related to the signer user
		            'userId'   => $user['id'], // userid from the users table
		            'userName' => $username, // User name
		        ]
		    ];   

		    $secretKey = JWT_SECRET;

			$jwt = JWT::encode($data, $secretKey);

			$response = ['token' => $jwt, 'user' => $user];
		}
		else{

			$error = "Username / Password doesn't match";
			$response =  ['error' => $error];
		}

		
	
		echo json_encode($response);
	}

	public function getAuthenticatedUser() {

		try{

			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);
			$jwt = $request->token;


			$secretKey = JWT_SECRET;

			$response = JWT::decode($jwt, $secretKey, array('HS256'));

			echo json_encode($response->data);		
		}
		catch(Exception $e){

			$message = $e->getMessage();

			$res = ['message' => $message];

			echo json_encode($res);
		
		}
	}
	
}