<?php

namespace CmsMvc\Controllers;

use CmsMvc\Services\View;
use CmsMvc\Services\Password;


	class BaseController {

		protected $Model;


		public function __construct() {


		}

		public function store(){

			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);

			$input = $this->objectToArray($request->input);

			if($this->Model->insert($input)) {
					
					return true;
			}

		}



		public function update($id) {

	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);

			$input = $this->objectToArray($request->input);

			if($this->Model->update($id, $input)){

				return true;
			}
			else {

				$_SESSION['error']['message'] = "Something went wrong !";

			}

		}

		public function objectToArray($object){

			$array = [];
			foreach ($object as $key => $value) {
				$array[$key] = $value;
			}

			return $array;
		}

		public function delete($id){

			$result = $this->Model->destroy($id);

			echo json_encode($result);
		}



	}