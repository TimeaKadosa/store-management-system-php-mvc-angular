<?php

namespace CmsMvc\Controllers;

use CmsMvc\Controllers\BaseController;
use CmsMvc\Models\UserModel;
use CmsMvc\Models\CompanyModel;
use CmsMvc\Models\UserTypesModel;
use CmsMvc\Services\View;
use CmsMvc\Services\Password;
use CmsMvc\Services\Filehandler;

	class UserController extends BaseController {


		public function __construct() {

			$this->Model = new UserModel();
			parent::__construct();
		}

		public function index(){

			$users = $this->Model->fetchAll();

			$userTypesModel = new UserTypesModel();
			$usertypes = $userTypesModel->fetchAll();
			
			$response = ['users' => $users, 'usertypes' => $usertypes];
			echo json_encode($response);	
		}

		public function show($id) {


			$user[] = $this->Model->fetch($id);
			echo json_encode($user);	

		}

		public function signup(){

			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata);

			$user = $this->objectToArray($request->input->user);
			$company = $this->objectToArray($request->input->company);


			$companyModel = new CompanyModel();

			if($companyModel->insert($company)) {
					
					$user['company_id'] = $companyModel->getLastInsertId();
					$this->Model->insert($user);
			}
			
			if ($this->Model->error) {
				$response['error'] = $this->Model->error;
			}


			echo json_encode($response);
		}


	}