<?php
	/*
	 * System config and initialization
	 */
	
	include ('../init.inc.php');
	



	use CmsMvc\Services\Router;
 
 	$route = Router::getRoute();

 	if($route == "/") {
		// include base page
		include ('views/index.php');
 	}

	/*
	 * Authentication
	 */

	Router::setRoute('/auth', 'AuthController@authenticate','POST'); 
	Router::setRoute('/getAuthUser', 'AuthController@getAuthenticatedUser','POST'); 

	/*
	 * User
	 */

	Router::setRoute('/user', 'UserController@index','GET'); 				
	Router::setRoute('/user/:id', 'UserController@show','GET'); 		
	Router::setRoute('/user/:id', 'UserController@update','POST'); 	
	Router::setRoute('/user', 'UserController@store','POST'); 				
	Router::setRoute('/user/:id/delete', 'UserController@delete','GET'); 	
	Router::setRoute('/user/signup', 'UserController@signup','POST'); 	
	
	Router::setRoute('/user/:id/upload', 'UserController@upload','POST'); 	




