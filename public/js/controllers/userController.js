myApp.controller('userController', userController);

userController.$inject = ['$scope', '$location', 'userModel','$timeout','data','toaster','$uibModal','$route','DTOptionsBuilder', 'DTColumnDefBuilder','$cookies', 'companyModel'];

    function userController ($scope, $location, userModel,$timeout, data, toaster, $uibModal,$route,DTOptionsBuilder, DTColumnDefBuilder,$cookies, companyModel) {



        if(data && data.users != undefined) {
  
            data.users.success(function(response) {

                $scope.users = response.users;
                $scope.usertypes = response.usertypes;
                $scope.showUsers = true; 

            });


        }


        if(data && data.user != undefined) {


            data.user.success(function(response){

                if(response[0]) {

                    $scope.user = response[0];
                }
                else{
                    $scope.user = {};
                }
            });

        }

        
        /* variables */
        angular.extend($scope, {
 
            filters : {},
            dataUser: {},
            idSelected : null,
            required: false,
            company: {},
            user: {},

            data:{},

               

            user: {
                attr: {}
            },
            usertypes: {
                selected: {}
            }
 

    
        });

        /* functions */
        angular.extend($scope, {
            

            createUser: function() {

                if ($scope.data.user.password_c) {

                    delete $scope.data.user.password_c;    
                }
              
                userModel.createUser($scope.data.user).then(function(response){

                        toaster.pop('success', "User was added successfully "); 
                
                    
                });
              
            },

            signup: function() {

                userModel.signup($scope.data);
            },


            updateUser: function() {
    
                    console.log($scope.data.user);

                    userModel.updateUser($scope.data.user).then(function(response){

                        if (response.data.error) {


                            toaster.pop('error', response.data.error);    
                        }
                        else{

                            userModel.getAllUsers().then(function(response){
                                    $scope.users = response.data.users;
                                });

                            toaster.pop('success', "User was edited successfully "); 
                        }
                    });
   

            },

            removeUser: function(id){

                userModel.removeUser(id).then( function(response){

                    if(response.data == true){

                        toaster.pop('success', "User was deleted successfully "); 
                    }

                    else {

                        toaster.pop('error', "There was a problem "); 
                    }

     
                });
            },


          

            openCalendar: function() {
                
                $scope.isOpen = true;
            },

            cancel : function(id) {

                userModel.getUser(id).then(function(response){

                    $scope.user = response.data[0];
                });
                  
            },


            showUserData: function(user){

                // hide addUserForm
                $scope.addUser = false;
                // show enable to show editUserForm
                $scope.editUser = true;
                // show the current user's data
                userModel.getUser(user.id).then(function(response){

                    $scope.data.user = response.data[0];
             
                });

  
                // get the current user's type to show
                $scope.currentUser = user;
                angular.forEach($scope.usertypes, function(value, i) {

                    if ( value.id == $scope.currentUser.usertype_id) {

                        $scope.usertypes.selected = $scope.usertypes[i];
                    }
                });

                // show the form
                $scope.showForm = true;
            },

            hideForm: function() {

                // hide edit/add form
                $scope.editUser = false;
                $scope.addUser = false;
                //hide the form 
                $scope.showForm = false;
            },

            showAddUser: function() {
  
                $scope.isRequired = true;        

                $scope.data = {};
                $scope.usertypes.selected = false;
                // hide edit form
                $scope.editUser = false;
                // show the form
                $scope.showForm = true;
                // show add user form
                $scope.addUser = true;

            },  

        });


    }





