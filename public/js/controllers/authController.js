myApp.controller('authController', ['$scope','$cookieStore','$location','$auth' ,'authModel' ,'data','$http' , '$rootScope', '$window', 'toaster','$cookies',
	function($scope,$cookieStore,$location,$auth, authModel,data, $http, $rootScope, $window, toaster, $cookies){
	
	
	if(data && data.users != undefined) {

			// if so we get the data for the user
            data.users.success(function(response){

                $scope.users = response;
            });

    }

	
	angular.extend($scope, {

	        login: function() {

	        	var credentials = {
	                username: $scope.auth.username,
	                password: $scope.auth.password
	            };

	        	authModel.login(credentials);
	        },

			forgotPassword: function(){

				var credentials = {
					email: $scope.email
				};

				authModel.forgotPassword(credentials).then(function(response){

					toaster.pop('success', "Password reset email has been sent");
					$location.path('/');
					
				}, function(error){
					console.log(error);
				});
			},

			resetPassword: function() {
				
				authModel.resetPassword($scope.user).then(function(response){
					console.log(response);
				});
			}

	});
   

}]);