myApp.controller('globalController', ['$scope', 'userModel','Page','$filter', function ($scope, userModel, Page,$translate,$filter) {

    angular.extend($scope, {
        app: {
            name: 'Store Management System'
        }
    });

    $scope.global = {};
    $scope.global.navUrl = templateUrl+'/partials/nav.html';
    $scope.Page = Page;
    $scope.config = {};
    $scope.config.manager = 5;

    $scope.config.colors ={
        0:'primary',
        1:'accent',
        2:'warn',
        3:'info',
        4:'success',
        5:'warning',
        6:'danger',

    }

    //$scope.auth = userModel.getAuthStatus();
    /* FILTERS */


    // DROP DOWNBOX SETTINGS-----------------------------
    $scope.status = {  isopen: false };
    $scope.toggled = function(open) { $log.log('Dropdown is now: ', open); };
    $scope.toggleDropdown = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
    };

    $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
    // DROP DOWNBOX SETTINGS-----------------------------



    //   SUMMER NOTE OPTIONS----------------------------
    $scope.summernote_options = {
        height: 200,
        focus: false,
        toolbar: [
            ['edit',['undo','redo']],
            ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
            ['fontclr', ['color']],
            ['insert', ['link','hr']],
        ]
    };
    //   SUMMER NOTE OPTIONS----------------------------

}]);

myApp.filter('capitalize', function () {
    return function (input, all) {
        var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
        return (!!input) ? input.replace(reg, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) : '';
    }
});