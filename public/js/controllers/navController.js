myApp.controller('navController', ['$scope', '$location', 'userModel', 'authModel', function($scope, $location, userModel, authModel) {
    /*Variables*/
    angular.extend($scope, {
        //user: authModel.getAuthenticatedUser(),
        navUrl: [
        {
            name: 'home',
            url: '/dashboard',
            icon: 'apps',
        
        }, 
        {
            name: 'users',
            url: '/users',
            icon: 'supervisor_account',
        }, 
        {
            name: 'customers',
            url: '/customers',
            icon: 'supervisor_account',
        }, 
        {
            name: 'products',
            url: '/products',
            icon: 'supervisor_account',
        }, 
        {
            name: 'orders',
            url: '/orders',
            icon: 'supervisor_account',
        },
        {
            name: 'stores',
            url: '/stores',
            icon: 'supervisor_account',
        }, 
        ]
    });

    /*Methods*/
    angular.extend($scope, {
        logout: function() {
            authModel.logout();
            $location.path('/');
        },
        checkActiveLink: function(routeLink) {
            if ($location.path() == routeLink) {
                return 'make-active';
            }
        }
    });
}]);
