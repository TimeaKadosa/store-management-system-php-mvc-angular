/*This is the main file where angular is defined*/
var myApp = angular.module('myApp',
    [
        'ngRoute',
        'ngCookies',
        'ngAnimate',
        'ui.bootstrap',
     
        'toaster',
        'satellizer',
        'datatables',
    ]);




myApp.config(['$routeProvider', '$locationProvider','$authProvider','$httpProvider',
    function($routeProvider, $locationProvider, $authProvider, $httpProvider) {

        //var authenticated = false;

        $authProvider.loginUrl = '/auth';

        $routeProvider.when('/', {
            templateUrl: templateUrl+'users/login.html',
            controller: 'authController',

            resolve: {
                data: function(userModel) {
                    return {
                        //users: userModel.getAllUsers()
                    };

                }
            },
            authenticated: false
        });

        $routeProvider.when('/signup', {
            templateUrl: templateUrl+'users/signup.html',
            controller: 'userController',
            resolve: {
                data: function(userModel) {
                    return {
                        //users: userModel.getAllUsers()
                    };

                }
            },
            authenticated: false
        });


        $routeProvider.when('/users', {
            templateUrl: templateUrl+'users/index.html',
            controller: 'userController',
            resolve: {
                data: function(userModel) {
                    return {
                        users: userModel.getAllUsers()
                    };

                }
            },
            pageTitle: 'users',
            authenticated: true
        });

         $routeProvider.when('/users/edit/:id', {
            templateUrl:  templateUrl+'/users/edit.html',
            controller: 'userController',
            resolve: {
                data: function(userModel, $route) {
                    return {
                        user: userModel.getUser($route.current.params.id)
                    };

                }
            },
            pageTitle: 'users',
            authenticated: true
        });

        $routeProvider.when('/users/add', {
            templateUrl:  templateUrl+'/users/add.html',
            controller: 'userController',
            resolve: {
                data: function(userModel, $route) {
                    return { };

                }
            },
            pageTitle: 'users',
            authenticated: true
        });



      

        $routeProvider.when('/dashboard', {
            templateUrl: templateUrl+'dashboard.html',
 
            authenticated: true
        });

        $routeProvider.otherwise('/');
    }
]);


myApp.factory('Page', function(){
    var title = '';
    return {
        title: function() { return title; },
        setTitle: function(newTitle) { title = newTitle;}
    };
});

myApp.run(["$rootScope", "$location", 'userModel', 'Page','$filter','authModel', '$cookies',
    function($rootScope, $location, userModel, Page,$filter ,authModel, $cookies) {
        $rootScope.$on("$routeChangeStart",

            function(event, next, current) {

                var token = localStorage.getItem('satellizer_token');

                authModel.getAuthenticatedUser(token);
                 
                $rootScope.showSideBar =  false;
                
                var user = JSON.parse(localStorage.getItem('auth'));
                
                if(next.$$route != undefined){

                   if(next.$$route.authenticated){
                    
                        $rootScope.showSideBar =  true;
                        // checks if the userModel getAuthStatus returns true or false
                        if(!user){
                            // if false , takes the user back to the login screen
                            $location.path('/');

                        }
                    }

                    // if the user trying to go to the login screen and already logged in
                    if(next.$$route.originalPath == '/' ){
                        // he should be directed back to the same page where he was previously
                        if(user){
                            $location.path(current.$$route.originalPath);
                        }
                    }
                }
            
            });
    }
]);
