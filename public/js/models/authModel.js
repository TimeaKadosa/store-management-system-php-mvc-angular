myApp.factory('authModel', ['$http','$auth','$location','$cookieStore','toaster',  function($http, $auth,$location, $cookieStore, toaster){
	

	// return an object
	return {

		login: function(credentials) {

	            

	            // Use Satellizer's $auth service to login
	            $auth.login(credentials).then(function(data) {


	                return $http({
           
		                headers: {
		                    'Content-Type': 'application/json'
		                },
		                
		                url: baseUrl + '/auth',
		                method: "POST",
		                data: {

		                    username: credentials.username,
		                    password: credentials.password,
		                },
		            })

	                
	            }).then(function(response) {

					if (response.data.error) {

						loginError = true;
		                loginErrorText = response.data.error;

		                var alertText = '<div class="alert alert-danger"><strong>' + loginErrorText +'</strong></div>';
		                angular.element(document.querySelector('#alert')).append(alertText);

	                	$location.path('/');
	                }
	                else{
	                	// Stringify the returned data to prepare it
	                	// to go into local storage
		                var user = JSON.stringify(response.data.user);

		                // Set the stringified user data into local storage
						localStorage.setItem('user', user);
						localStorage.setItem('auth' , true);
	               		$location.path('dashboard');

	                }

				
            });


        },

 

		getAuthenticatedUser: function(token) {

				return $http({
           
		                headers: {
		                    'Content-Type': 'application/json',
		              
		                },
		                
		                url: baseUrl + '/getAuthUser',
		                method: "POST",
		                data: {

		                    token: token
		                },

	            }).then(function(response) {

						// if there was an error during the authentication
						if (response.data.message) {

							
							localStorage.clear();
							localStorage.setItem('auth' , false);
							//$location.path('/');
						}

						else{
							console.log(response);
						}	
	            	});
		},

		logout : function() {

	        	
	            $auth.logout().then(function() {

	                localStorage.clear();
	                localStorage.setItem('auth', false);


	                $location.path('/');
	            });
        }
	};

}]);