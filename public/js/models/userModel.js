myApp.factory('userModel', ['$http', '$cookies','$location','toaster', function($http, $cookies,$location, toaster) {


    return {


        getAllUsers : function() {
            return $http.get(baseUrl + '/user');
        },


        getUser : function(id) {
            return $http.get(baseUrl + '/user/' + id);
        },

        updateUser: function(data) {

            return $http({
                
                headers: {
                    'Content-Type': 'application/json'
                },
                
                url: baseUrl + '/user/' + data.id,
                method: "POST",
                data: {

                    input: data

                },

        

            });
        },

        createUser: function(data){

             return $http({
                
                headers: {
                    'Content-Type': 'application/json'
                },
                
                url: baseUrl + '/user',
                method: "POST",
                data: {
                    input: data
                },

            });
        },

        signup: function(data){

                return $http({
                    
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    
                    url: baseUrl + '/user/signup',
                    method: "POST",
                    data: {
                        input: {
                            user: data.user,
                            company: data.company
                        } 
                    },

                }).then(function(response) {

                    if (response.data.error) {

                        toaster.pop('error', response.data.error); 
                    }
                    else{
                       
                        $location.path('/');
                        toaster.pop('success', "The account was created successfully"); 
                    }

                
            });
        },

        removeUser: function(id) {

                return $http.get(baseUrl + '/user/' + id +'/delete');

        },
        


    };

    
}]);
