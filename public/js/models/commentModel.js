myApp.factory('commentModel', ['$http', '$cookies','$location','toaster', function($http, $cookies,$location, toaster) {


    return {

        createComment: function(data){


            return $http({
                
                headers: {
                    'Content-Type': 'application/json'
                },
                
                url: baseUrl + '/comment',
                method: "POST",
                data: {

                    user_id: data.user.id, 
                    object_id: data.object_id, 
                    object_type: data.object_id, 
                    comment: data.comment,
                    active: data.active,
                },

        

            });
        },

        getAllComments: function(){
            return $http.get(baseUrl + '/comment');
        },
        
    };

    
}]);
