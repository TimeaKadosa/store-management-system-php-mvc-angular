<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <title>CmsMvc | by Timea Kadosa</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="images/logo.png">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">

    <link rel="shortcut icon" sizes="196x196" href="images/logo.png">

    <!-- theme style -->
    <link rel="stylesheet" href="template_assets/animate.css/animate.min.css" type="text/css">
    <link rel="stylesheet" href="template_assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="template_assets/material-design-icons/material-design-icons.css" type="text/css">


    <link rel="stylesheet" href="template_assets/bootstrap/dist/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="template_assets/bootstrap-additions/dist/bootstrap-additions.min.css" type="text/css">

    <link rel="stylesheet" href="template_assets/styles/app.css">
    <link rel="stylesheet" href="template_assets/styles/font.css" type="text/css">
    <link rel="stylesheet" href="template_assets/styles/glyphicons.css" type="text/css">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="">

    <!-- Datatables --> 
    <link rel="stylesheet" href="bower_components/datatables/media/css/jquery.dataTables.min.css">
    <!-- toastr -->
    <link href="bower_components/AngularJS-Toaster/toaster.min.css" rel="stylesheet" />  

    <script>
        var baseUrl = window.location.origin;
        var templateUrl = baseUrl + "/views/";
    </script>
</head>
<body>
<div class="app" id="app" ng-controller="globalController">

<toaster-container></toaster-container>
    <!-- ############ LAYOUT START-->

    <!-- aside -->
    <div id="aside" class="app-aside modal fade nav-dropdown"
         ng-class="{'folded': app.setting.folded}"
         ng-hide!="auth" class="ng-hide"
         ng-controller="navController"
         ng-if="showSideBar" >
        <!-- fluid app aside -->
        <div class="left navside black dk" layout="column">
            <div class="navbar no-radius">
                <div ng-include="'views/partials/navbar.brand.text.html'"></div>
            </div>
            <div flex-no-shrink class="b-t">
                <div ng-include="'views/partials/aside.top.html'"></div>
            </div>
            <div flex class="hide-scroll">
                <nav class="scroll nav-light nav-stacked">
                    <div ng-include="'views/partials/nav.html'" ></div>
                </nav>
            </div>
            <div flex-no-shrink="">
                <nav ui-nav="" class="ng-scope">
                    <ul class="nav">
                        <li>
                            <div class="b-b b m-t-sm"></div>
                        </li>
                        <li class="no-bg">
                            <a href="javascript:void(0)" ng-click="logout()">
                                <span class="nav-icon">
                                    <i class="material-icons"></i>
                                </span>
                                <span class="nav-text">Logout</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>


        </div>
    </div>
    <!-- / -->

    <!-- content -->
    <div id="content" class="app-content box-shadow-z0" role="main">
        <div class="app-header white box-shadow">
            <div ng-if="showSideBar" ng-include="'views/partials/header.html'"></div>
        </div>

        <div ui-view class="app-body" id="view">
            <!-- ############ PAGE START-->

            <div ng-view></div>

            <!-- ############ PAGE END-->

        </div>

        <div class="app-footer" ng-show="auth"  >
            <div ng-include="'views/partials/footer.html'"></div>
        </div>
    </div>
    <!-- / -->

        <!-- ############ LAYOUT END-->

</div>


<script src="bower_components/jquery/dist/jquery.js"></script>

<script src="bower_components/angular/angular.js"></script>
<script src="bower_components/angular-animate/angular-animate.min.js"></script>
<script src="bower_components/ui-bootstrap/ui-bootstrap-tpls-1.1.0.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="bower_components/angular-route/angular-route.js"></script>
<script src="bower_components/angular-cookies/angular-cookies.js"></script>
<script src="bower_components/angular-translate/angular-translate.min.js"></script>
<script src="bower_components/angular-translate-handler-log/angular-translate-handler-log.min.js"></script>
<script src="bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
<!-- <script src="bower_components/angular-ui-select/dist/select.js"></script> -->

<!-- JWT AUTH -->
<script type="text/javascript" src="bower_components/satellizer/satellizer.js"></script>

<!-- Datatables -->
<script src="bower_components/angular-datatables/dist/angular-datatables.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

<!-- TOASTER -->
<script src="bower_components/angularJS-Toaster/toaster.js"></script>

<script src="js/app.js"></script>
<!-- Models -->
<script src="js/models/userModel.js"></script>
<script src="js/models/authModel.js"></script>
<script src="js/models/companyModel.js"></script>

<!-- Controllers -->
<script src="js/controllers/globalController.js"></script>

<script src="js/controllers/navController.js"></script>
<script src="js/controllers/userController.js"></script>
<script src="js/controllers/authController.js"></script> 
<script src="js/controllers/companyController.js"></script> 